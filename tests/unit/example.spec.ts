import { expect } from "chai";
import { flushPromises, shallowMount } from "@vue/test-utils";
import App from "@/App.vue";
import InputText from "primevue/inputtext";
import MockAPI from "@/http/MockAPI";
import sinon from "sinon";
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import Button from "primevue/button";
import Textarea from "primevue/textarea";
import Toast from "primevue/toast";
import Dialog from "primevue/dialog";
import ConfirmDialog from "primevue/confirmdialog";
import PrimeVue from "primevue/config";
import ToastService from "primevue/toastservice";
import ConfirmService from "primevue/confirmationservice";

describe("App.vue", () => {
  it("Check InputText onChange", async () => {
    const axiosStub = sinon.stub(MockAPI, "get");
    axiosStub.returns(
      Promise.resolve({
        data: [
          {
            createdBy: "Chris Turner",
            description:
              "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
            id: "1",
          },
          {
            createdBy: "Lowell Runolfsson",
            description:
              "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
            id: "2",
          },
          {
            createdBy: "Lamar Kunze",
            description:
              "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
            id: "3",
          },
          {
            createdBy: "Rosemarie Corkery",
            description:
              "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
            id: "4",
          },
        ],
        status: 200,
        statusText: "OK",
        headers: {},
        config: {},
      }).then()
    );
    const wrapper = shallowMount(App, {
      components: {
        DataTable,
        Column,
        Button,
        InputText,
        Textarea,
        Toast,
        Dialog,
        ConfirmDialog,
      },
      global: {
        plugins: [PrimeVue, ToastService, ConfirmService],
      },
    });
    wrapper.findComponent(InputText).vm.$emit("change");
    await flushPromises();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.products).deep.eq([
      {
        createdBy: "Chris Turner",
        description:
          "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
        id: "1",
      },
      {
        createdBy: "Lowell Runolfsson",
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        id: "2",
      },
      {
        createdBy: "Lamar Kunze",
        description:
          "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
        id: "3",
      },
      {
        createdBy: "Rosemarie Corkery",
        description:
          "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
        id: "4",
      },
    ]);
    expect(axiosStub.callCount).to.equal(2);
  });

  it("Check Function Filter", async () => {
    sinon.restore();
    const axiosStub = sinon.stub(MockAPI, "get");
    axiosStub.returns(
      Promise.resolve({
        data: [
          {
            createdBy: "Chris Turner",
            description:
              "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
            id: "1",
          },
          {
            createdBy: "Lowell Runolfsson",
            description:
              "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
            id: "2",
          },
          {
            createdBy: "Lamar Kunze",
            description:
              "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
            id: "3",
          },
          {
            createdBy: "Rosemarie Corkery",
            description:
              "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart",
            id: "4",
          },
        ],
        status: 200,
        statusText: "OK",
        headers: {},
        config: {},
      }).then()
    );
    const wrapper = shallowMount(App, {
      components: {
        DataTable,
        Column,
        Button,
        InputText,
        Textarea,
        Toast,
        Dialog,
        ConfirmDialog,
      },
      global: {
        plugins: [PrimeVue, ToastService, ConfirmService],
      },
    });
    wrapper.findComponent(InputText).vm.$emit("change");
    wrapper.vm.name = "Lamar";
    await flushPromises();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.products).deep.eq([
      {
        createdBy: "Lamar Kunze",
        description:
          "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design",
        id: "3",
      },
    ]);
    expect(axiosStub.callCount).to.equal(2);
  });

  it("Check Add Product", async () => {
    sinon.restore();
    const axiosStub = sinon.stub(MockAPI, "post");
    const mockProduct = {
      createdBy: "Chris Turner",
      description:
        "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality",
      id: 0,
    };

    axiosStub.withArgs(mockProduct).returns(
      Promise.resolve({
        status: 200,
        statusText: "OK",
        data: [mockProduct],
        headers: {},
        config: {},
      }).then()
    );

    const wrapper = shallowMount(App, {
      components: {
        DataTable,
        Column,
        Button,
        InputText,
        Textarea,
        Toast,
        Dialog,
        ConfirmDialog,
      },
      global: {
        plugins: [PrimeVue, ToastService, ConfirmService],
      },
    });

    wrapper.vm.name = mockProduct.createdBy;
    wrapper.vm.description = mockProduct.description;

    await wrapper.find("form").trigger("submit.prevent");
    await flushPromises();

    sinon.assert.calledOnce(axiosStub);
    sinon.assert.calledWith(axiosStub, sinon.match(mockProduct));
    expect(wrapper.vm.products).to.deep.include(mockProduct);

    axiosStub.restore();
  });
});
