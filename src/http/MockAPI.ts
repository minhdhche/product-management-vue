import axios from "axios";

export default class MockAPI {
  static async get(): Promise<any> {
    return await axios.get(
      "https://65642c20ceac41c0761d8c0f.mockapi.io/products"
    );
  }
  static async delete(id: any): Promise<any> {
    await axios.delete(
      "https://65642c20ceac41c0761d8c0f.mockapi.io/products/" + id
    );
  }
  static async post(product: any): Promise<any> {
    await axios.post(
      "https://65642c20ceac41c0761d8c0f.mockapi.io/products",
      product
    );
  }
  static async put(product: any): Promise<any> {
    await axios.put(
      "https://65642c20ceac41c0761d8c0f.mockapi.io/products/" + product.id,
      product
    );
  }
}
